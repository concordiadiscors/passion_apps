# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

%i(verticals categories courses).each do |records|
  json_file_path = Rails.root.join("db/seeds_support_files/#{records}.json")
  json_records = JSON.parse(File.read(json_file_path))
  instance_variable_set "@#{records}_data", json_records
end

verticals_attributes_mapping = {
  name: 'Name'
}

categories_attributes_mapping = {
  name: 'Name',
  state: 'State'
}

courses_attributes_mapping = {
  name: 'Name',
  author: 'Author',
  state: 'State'
}

def attributes_from_json_record(hash_record, mapping)
  mapping.each_with_object({}) do |(attr, json_attr), attributes|
    attributes[attr] = hash_record[json_attr]
  end
end

verticals =  ActiveRecord::Base.transaction do
  @verticals_data.map do |record|
    attributes = attributes_from_json_record(record, verticals_attributes_mapping)
    Vertical.create!(attributes)
  end
end

categories = ActiveRecord::Base.transaction do
  @categories_data.map do |record|
    base_attributes = attributes_from_json_record(record, categories_attributes_mapping)
    relation_index = record['Verticals'] - 1
    relation = { vertical: verticals[relation_index] }
    Category.create!(base_attributes.merge(relation))
  end
end

courses = ActiveRecord::Base.transaction do
  @courses_data.map do |record|
    base_attributes = attributes_from_json_record(record, courses_attributes_mapping)
    relation_index = record['Categories'] - 1
    relation = { category: categories[relation_index] }
    Course.create!(base_attributes.merge(relation))
  end
end





