class AddNameValidationTriggersOnDb < ActiveRecord::Migration[6.0]
  def up
    execute <<-SQL
      CREATE OR REPLACE FUNCTION check_taxonomic_name_uniqueness_func()
      RETURNS TRIGGER
      AS $$
      BEGIN
      IF exists(
              select 1 from (
               select id, name, 'categories' as tbl from categories
               union
               select id, name, 'verticals' as tbl from verticals
              ) all_taxonomies where all_taxonomies.name = NEW.name and (
                      all_taxonomies.id != NEW.id OR
                      all_taxonomies.tbl != TG_TABLE_NAME
              )

      ) THEN
         RAISE EXCEPTION 'taxonomy name must be unique';
      END IF;
      RETURN NEW;
      END;
      $$
      LANGUAGE plpgsql;

      CREATE TRIGGER validates_category_name_uniqueness_trigger
      BEFORE INSERT OR UPDATE ON categories
      FOR EACH ROW EXECUTE PROCEDURE check_taxonomic_name_uniqueness_func();

      CREATE TRIGGER validates_vertical_name_uniqueness_trigger
      BEFORE INSERT OR UPDATE ON verticals
      FOR EACH ROW EXECUTE PROCEDURE check_taxonomic_name_uniqueness_func();
    SQL
  end

  def down
    execute <<-SQL
      DROP TRIGGER validates_category_name_uniqueness_trigger ON categories;
      DROP TRIGGER validates_vertical_name_uniqueness_trigger ON verticals;
      DROP FUNCTION check_taxonomic_name_uniqueness_func;
    SQL
  end
end
