class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :categories do |t|
      t.references :vertical, null: false, foreign_key: true
      t.string :name
      t.string :state

      t.timestamps
    end
    add_index :categories, :name, unique: true
  end
end
