Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  Rails.application.routes.draw do
    namespace :api, constraints: { format: 'json' } do
      namespace :v1 do
        resources :verticals, except: [:new, :edit] do
          resources :categories, except: [:new, :edit]
        end
        resources :courses, except: [:new, :edit]
        post 'authenticate', to: 'authentications#authenticate'
      end
    end
end

end
