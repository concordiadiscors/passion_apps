class CourseSerializer < ActiveModel::Serializer
  attributes :id, :name, :state, :author

  belongs_to :category
end
