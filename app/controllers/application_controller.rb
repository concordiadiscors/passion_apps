class ApplicationController < ActionController::API

  before_action :get_authenticated_user

  rescue_from ActiveRecord::RecordNotFound,       with: :not_found_response
  rescue_from ActionController::ParameterMissing, with: :generic_unprocessable_entity_response

  def render_model_errors(model)
    render json: { errors: model_errors_hash(model) },
           status: :unprocessable_entity
  end

  def model_errors_hash(model)
    model.errors.each_with_object([]) do |(attribute, message), arr|
      arr << {
        status: "422",
        attribute: attribute,
        title: message
      }
    end
  end

  def unauthorised_response
    render json: { errors: [ {
      status: "401",
      title: "Unauthorized" } ]
    }, status: :unauthorized
  end

 private

   def generic_unprocessable_entity_response
    render json: { errors: [ {
      status: "422",
      source: "",
      title: "Unprocessable Entity" } ]
    }, status: :unprocessable_entity
  end

  def not_found_response
    render json: { errors: [ {
      status: "404",
      source: "",
      title: "Record Not Found" } ]
    }, status: :not_found
  end

  def get_authenticated_user
    unauthorised_response unless (@current_user = request_authorisation&.user)
  end

  def request_authorisation
    return unless header_authorisation.present?
    jwt_token = header_authorisation.split(' ').last
    ApiRequestAuthorisation.new(jwt_token)
  end

  def header_authorisation
    request.headers['Authorization']
  end

end
