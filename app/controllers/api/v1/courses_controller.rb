class Api::V1::CoursesController < ApplicationController

  before_action :find_course, only: [:show, :update, :destroy]

  def index
    @courses = Course.all
    render json: @courses
  end

  def show
    render json: @course
  end

  def create
    course = Course.new(course_params)
    if course.save
      NewRecordNotification.new(course).send_sqs
      head :created
    else
      render_model_errors course
    end
  end

  def update
    if @course.update(course_params)
      head :no_content
    else
      render_model_errors @course
    end
  end

  def destroy
    @course.destroy
    head :no_content
  end

  private

  def find_course
    @course = Course.find(params[:id])
  end

  def course_params
      params.require(:course)
        .permit(:category_id, :state_event, :name, :author)
  end

end
