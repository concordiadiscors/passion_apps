class Api::V1::CategoriesController < ApplicationController

  before_action :find_vertical, only: [:index, :show, :update, :destroy]
  before_action :find_category, only: [:show, :update, :destroy]

  def index
    @categories = @vertical.categories
    render json: @categories
  end

  def show
    render json: @category
  end

  def create
    category = Category.new(category_params)
    if category.save
      NewRecordNotification.new(category).send_sqs
      head :created
    else
      render_model_errors category
    end
  end

  def update
    if @category.update(category_params)
      head :no_content
    else
      render_model_errors @category
    end
  end

  def destroy
    @category.destroy
    head :no_content
  end

  private

  def find_category
    @category = @vertical.categories.find(params[:id])
  end

  def find_vertical
    @vertical = Vertical.find(params[:vertical_id])
  end

  def category_params
      params.require(:category)
        .permit(:vertical_id, :state_event, :name)
  end

end
