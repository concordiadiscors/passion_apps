class Api::V1::VerticalsController < ApplicationController

  before_action :find_vertical, only: [:show, :update, :destroy]

  def index
    @verticals = Vertical.all
    render json: @verticals
  end

  def show
    render json: @vertical
  end

  def create
    vertical = Vertical.new(vertical_params)
    if vertical.save
      NewRecordNotification.new(vertical).send_sqs
      head :created
    else
      render_model_errors vertical
    end
  end

  def update
    if @vertical.update(vertical_params)
      head :no_content
    else
      render_model_errors @vertical
    end
  end

  def destroy
    @vertical.destroy
    head :no_content
  end

  private

  def find_vertical
    @vertical = Vertical.find(params[:id])
  end

  def vertical_params
      params.require(:vertical).permit(:name)
  end

end
