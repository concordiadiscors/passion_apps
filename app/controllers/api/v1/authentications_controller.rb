class Api::V1::AuthenticationsController < ApplicationController
  skip_before_action :get_authenticated_user
  before_action :user_from_code

  def authenticate
    if @user
      render json: { auth_token: jwt_token }, status: :created
    else
      unauthorised_response
    end
  end

  private

  def jwt_token
    JsonWebToken.encode(payload_for_token)
  end

  def payload_for_token
    { user_id: @user.id }
  end

  def user_from_code
   return unless github_user
   @user = User.find_by(email: github_user.email )
  end

  def github_user
    return unless github_code.present?
    ::OauthClient::Github.user_from_auth_code(github_code)
  end

  def github_code
    authentication_params[:github_authorisation_code]
  end

  def authentication_params
      params.require(:authentication)
        .permit(:github_authorisation_code)
  end

end
