class NewRecordNotification

  NOTIFICATION_SQS_QUEUE = 'some_queue_name'.freeze

  attr_reader :record
  delegate :id, :class, :name, to: :record, prefix: true

  def initialize(record)
    @record = record
  end

  def send_sqs
    # would  need to raise & handle exceptions here as well
    Aws::Sqs.send_sqs(
      queue: NOTIFICATION_SQS_QUEUE,
      message_body: message_body,
      message_attributes: message_attributes
    )
  end

  private

  def message_body
    "A new #{readable_record_type} has been created: #{record_name}"
  end


  def message_attributes
    {
      'Type' => {
        string_value: record.model_name,
        data_type: 'String'
      },
      'ID' => {
        string_value: record_id.to_s,
        data_type: 'Number'
      },
      'Name' => {
        string_value: record_name,
        data_type: 'String'
      },
    }
  end

  def readable_record_type
    record.model_name.human
  end

end
