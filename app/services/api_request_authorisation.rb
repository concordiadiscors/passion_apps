class ApiRequestAuthorisation

  def initialize(jwt_token)
    @jwt_token = jwt_token
  end

  def user
    return if expired?
    User.find(user_id)
  end

  private

  def user_id
    decoded_jwt_token[:user_id]
  end

  def expiration_timestamp
    DateTime.strptime(decoded_jwt_token[:expiration].to_s, '%s')
  end

  def expired?
    Time.current.after? expiration_timestamp
  end

  def decoded_jwt_token
    @decoded_auth_token ||= JsonWebToken.decode(@jwt_token)
  end

end
