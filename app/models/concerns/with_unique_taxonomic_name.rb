module WithUniqueTaxonomicName
  extend ActiveSupport::Concern

  @@taxonomic_classes = []

  included do
    @@taxonomic_classes << self
    validate :name_unique_across_all_taxonomies
  end

  def self.get_taxonomic_classes
      @@taxonomic_classes
  end

  private

  def name_unique_across_all_taxonomies
    return if name.blank?
    return unless has_duplicate?
    errors.add(:name, 'has already been taken')
  end

  def has_duplicate?
    query_result.first["duplicates_count"].nonzero?
  end

  def query_result
    self.class.connection.select_all(main_query)
  end

  def main_query
    "SELECT COUNT(*) AS duplicates_count FROM (#{union_query}) all_taxonomic_records WHERE #{conditional_statement}"
  end

  def union_query
    @@taxonomic_classes.map do |klass|
      "SELECT id, name, '#{klass.name}' AS class FROM #{klass.table_name}"
    end.join("\r\n UNION \r\n")
  end

  def conditional_statement
    "name = '#{name}' #{exclude_self_conditional};"
  end

  def exclude_self_conditional
    return unless persisted?
    " AND (id != #{self.id} OR class != '#{self.class.name}')"
  end
end
