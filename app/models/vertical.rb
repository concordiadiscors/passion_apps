class Vertical < ApplicationRecord
  include WithUniqueTaxonomicName

  #relations
  has_many :categories, dependent: :destroy

  # validations
  validates :name, presence: true
end
