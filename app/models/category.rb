class Category < ApplicationRecord
  include WithUniqueTaxonomicName

  # relationships
  belongs_to :vertical
  has_many :courses, dependent: :destroy

  # validations
  validates :name, presence: true

  # state machine
  state_machine :state, initial: :draft do
    event :activate do
      transition :draft => :active
    end

    event :archive do
      transition [:draft, :active] => :archived
    end
  end

end
