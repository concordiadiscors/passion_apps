class Course < ApplicationRecord
  # relationships
  belongs_to :category

  # validations
  validates :name, presence: true
  validates :author, presence: true

  # state machine
  state_machine :state, initial: :draft do
    event :activate do
      transition :draft => :active
    end

    event :archive do
      transition [:draft, :active] => :archived
    end

    state :active do
      validate :validate_active_category
    end

  end

  private

  def validate_active_category
    return if category.active?
    errors.add(:state, "can only be active if related category is itself active")
  end
end
