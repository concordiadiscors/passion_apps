RSpec.shared_context "with_authenticated_user" do
    let(:user) { create(:user) }
    let(:jwt) { JsonWebToken.encode({user_id: user.id}) }
    let(:headers_with_authorisation) { {"Authorization": jwt} }
end
