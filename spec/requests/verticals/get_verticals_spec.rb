require 'rails_helper'
require_relative "../shared"

describe "get verticals route", :type => :request do
  include_context 'with_authenticated_user'

  context "vertical exists" do
    let!(:verticals) { create_list :vertical, 9}

    before {get "/api/v1/verticals/",
                headers: headers_with_authorisation }

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns the correct number of records' do
      expect(json_data.length).to be(9)
    end

    it 'returns the correct attributes' do
        expect(json_data.first).to have_attribute(:name).with_value(verticals.first.name)
    end

  end
end
