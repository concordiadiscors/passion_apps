require 'rails_helper'
require_relative "../shared"

describe "get vertical route", :type => :request do
  include_context 'with_authenticated_user'

  context "vertical exists" do
    let!(:vertical) { create :vertical}

    before {get "/api/v1/verticals/#{vertical.id}",
                headers: headers_with_authorisation }

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns the correct record' do
      expect(json_data).to have_id(vertical.id.to_s)
      expect(json_data).to have_type('verticals')
    end

    it 'returns the correct attributes' do
      testable_attribute_keys = %i(name)
      testable_attribute_keys.each do |attr|
        expect(json_data).to have_attribute(:name).with_value(vertical.name)
      end
    end

  end

  context "vertical doesn't exists" do

    let(:inexistant_id) { Vertical.last&.id.to_i + rand(100..1000) }

    before {get "/api/v1/verticals/#{inexistant_id}",
                headers: headers_with_authorisation }

    it 'returns status code 404' do
      expect_status(404)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns a json 404 error' do
      expect(parsed_json).to have_json_404_error
    end

  end
end
