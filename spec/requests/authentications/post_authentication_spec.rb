require 'rails_helper'
describe "post authentication route", :type => :request do

  let!(:user) { create :user }
  let!(:valid_code) { 'some_authorisation_code' }
  let!(:invalid_valid_code) { 'some_made_up_authorisation_code' }

  let!(:token_request_params) do
    {
      :client_id => ENV['GITHUB_OAUTH_CLIENT_ID'],
      :client_secret => ENV['GITHUB_OAUTH_CLIENT_SECRET'],
      :code => valid_code
    }
  end

  let!(:invalid_token_request_params) do
    {
      :client_id => ENV['GITHUB_OAUTH_CLIENT_ID'],
      :client_secret => ENV['GITHUB_OAUTH_CLIENT_SECRET'],
      :code => invalid_valid_code
    }
  end


  let!(:jwt_for_user) {
    JsonWebToken.encode({user_id: user.id})
  }

  before do
    stub_request(:post, 'https://github.com/login/oauth/access_token')
    .with(body: token_request_params)
    .to_return do |request|
      {
        status: 200,
        headers:  {
          'Content-Type' => 'application/json',
        },
        body: {
          'access_token' => 'some_valid_token',
          'token_type' => 'Bearer',
          'expires_in' => 3600
        }.to_json
      }
    end

    stub_request(:post, 'https://github.com/login/oauth/access_token')
    .with(body: invalid_token_request_params)
    .to_return do |request|
      {
        status: 200,
        headers:  {
          'Content-Type' => 'application/json',
        },
        body: {
          'error' => 'bad_verification_code',
          'error_description' => 'The code passed is incorrect or expired.',
          'error_uri' => 'https://developer.github.com/apps/managing-oauth-apps/troubleshooting-oauth-app-access-token-request-errors/#bad-verification-code'
        }.to_json
      }
    end

    stub_request(:get, 'https://api.github.com/user')
    .with(headers: {"Authorization" => "Bearer some_valid_token"})
    .to_return do |request|
      {
        status: 200,
        body: { email: user.email }.to_json
      }
    end
  end

  context "authenticates with valid authorisation code" do
    before do
      post "/api/v1/authenticate", params: { authentication: { github_authorisation_code: valid_code } }
    end

    it 'returns status code 201' do
      expect_status(:created)
    end

    it 'does return a jwt token' do
      decoded_token = JsonWebToken.decode(parsed_json['auth_token'])
      expect(decoded_token[:user_id]).to eq(user.id)
    end

  end

  context "authenticates without  authorisation code" do
    before do
      post "/api/v1/authenticate", params: { authentication: { github_authorisation_code: nil } }
    end

    it 'returns status code 401' do
      expect_status(:unauthorized)
    end

  end

  context "authenticates with invalid authorisation code" do
    before do
      post "/api/v1/authenticate", params: { authentication: { github_authorisation_code: invalid_valid_code } }
    end

    it 'returns status code 401' do
      expect_status(:unauthorized)
    end

  end
end
