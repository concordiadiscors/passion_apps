require 'rails_helper'
require_relative "../shared"

describe "post category route", :type => :request do
  include_context 'with_authenticated_user'

  let!(:category) { create(:category) }
  let!(:vertical) { category.vertical }
  let!(:new_vertical) { create(:vertical, name: 'Lifestyle') }

  let!(:category_new_attributes) do
    {
      name: 'Ascetism',
      vertical_id: new_vertical.id
    }
  end


  context "tries to update category with valid params" do

    def put_valid_request
      put "/api/v1/verticals/#{vertical.id}/categories/#{category.id}",
          params: { category: category_new_attributes },
          headers: headers_with_authorisation
    end

    it 'returns status code 204' do
      put_valid_request
      expect_status(:no_content)
    end

    it 'does update the record' do
      expect do
        put_valid_request
      end.to change { category.reload.name }
      .and change { category.reload.vertical }
    end

  end

  context "tries to update invalid category" do

    context "invalid attribute" do
      let!(:invalid_new_category_attributes) do
        category_new_attributes.merge(name: '')
      end

      def put_request_with_invalid_attributes
        put "/api/v1/verticals/#{vertical.id}/categories/#{category.id}",
            params: { category: invalid_new_category_attributes },
            headers: headers_with_authorisation
      end

      it 'returns a valid JSON:API standard response' do
        put_request_with_invalid_attributes
        expect(parsed_json).to be_valid_json_api_response
      end

      it 'returns status code 422' do
        put_request_with_invalid_attributes
        expect_status(:unprocessable_entity)
      end

      it 'returns json error for invalid attribute' do
        put_request_with_invalid_attributes
        expect(parsed_json).to have_json_error_on_attribute(:name)
      end

      it 'does not update the record' do
        expect do
          put_request_with_invalid_attributes
        end.to not_change { category.reload.name }
        .and not_change { category.reload.vertical }
      end
    end

    context "missing required param" do
      let!(:invalid_new_category_attributes) do
        {some_irrelevant_param: 'really_irrelevant'}
      end

      def put_request_with_invalid_attributes
        put "/api/v1/verticals/#{vertical.id}/categories/#{category.id}",
            params: invalid_new_category_attributes,
            headers: headers_with_authorisation
      end

      it 'returns a valid JSON:API standard response' do
        put_request_with_invalid_attributes
        expect(parsed_json).to be_valid_json_api_response
      end

      it 'returns status code 422' do
        put_request_with_invalid_attributes
        expect_status(:unprocessable_entity)
      end

      it 'does not update the record' do
        expect do
          put_request_with_invalid_attributes
        end.to not_change { category.reload.name }
        .and not_change { category.reload.vertical }
      end
    end

  end

  context "category doesn't exists" do

    let(:inexistant_id) { Category.last&.id.to_i + rand(100..1000) }

    before do
      put "/api/v1/verticals/#{vertical.id}/categories/#{inexistant_id}",
          params: { category: category_new_attributes },
          headers: headers_with_authorisation
    end

    it 'returns status code 404' do
      expect_status(404)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns a 404 error' do
      expect(parsed_json).to have_json_404_error
    end

  end

end
