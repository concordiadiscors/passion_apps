require 'rails_helper'
require_relative "../shared"
describe "get categories route", :type => :request do

  include_context 'with_authenticated_user'

  context "category exists" do
    let!(:verticals)   { create_list :vertical, 3}
    let!(:first_vertical)   { verticals.first }
    let!(:categories) { create_list :category, 9, vertical: first_vertical}
    let!(:categories_second_set) { create_list :category, 15, vertical: verticals.second}

    before {get "/api/v1/verticals/#{first_vertical.id}/categories/", headers: headers_with_authorisation}

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns the correct number of records' do
      expect(json_data.length).to be(9)
    end

    it 'returns the correct attributes' do
      testable_attribute_keys = %i(name state)
      testable_attribute_keys.each do |attr|
        expect(json_data.first).to have_attribute(attr).with_value(categories.first.send(attr))
      end
    end

    it 'returns the correct relationships' do
      expect(json_data.first).to have_relationship(:vertical).with_data({
        'id' => first_vertical.id.to_s,
        'type' => 'verticals'
      })
    end

  end
end
