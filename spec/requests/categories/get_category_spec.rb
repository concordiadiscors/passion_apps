require 'rails_helper'
require_relative "../shared"

describe "get category route", :type => :request do
  include_context 'with_authenticated_user'

  let!(:vertical) { create :vertical }

  context "category exists" do
    let!(:category) { create :category, vertical: vertical }

    before {get "/api/v1/verticals/#{vertical.id}/categories/#{category.id}", headers: headers_with_authorisation}

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns the correct record' do
      expect(json_data).to have_id(category.id.to_s)
      expect(json_data).to have_type('categories')
    end

    it 'returns the correct attributes' do
      testable_attribute_keys = %i(name state)
      testable_attribute_keys.each do |attr|
        expect(json_data).to have_attribute(attr).with_value(category.send(attr))
      end
    end

    it 'returns the correct relationships' do
      expect(json_data).to have_relationship(:vertical).with_data({
        'id' => category.vertical_id.to_s,
        'type' => 'verticals'
      })
    end

  end

  context "category doesn't exists" do
    let(:inexistant_id) { Category.last&.id.to_i + rand(100..1000) }

    before {get "/api/v1/verticals/#{vertical.id}/categories/#{inexistant_id}", headers: headers_with_authorisation}

    it 'returns status code 404' do
      expect_status(404)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns a json 404 error' do
      expect(parsed_json).to have_json_404_error
    end

  end
end
