require 'rails_helper'
require_relative "../shared"

describe "post category route", :type => :request do
  include_context 'with_authenticated_user'

  let!(:vertical) { create :vertical }

  context "tries to creates valid category" do

    let!(:category_attributes) do
      build(:category, vertical: vertical).attributes
    end

    def post_valid_request
        post "/api/v1/verticals/#{vertical.id}/categories",
         params: { category: category_attributes },
         headers: headers_with_authorisation
    end

    it 'returns status code 201' do
      post_valid_request
      expect_status(:created)
    end

    it 'does create a new record' do
      expect do
        post_valid_request
      end.to change { Category.count }
    end

    it 'creates a notification' do
      expect_any_instance_of(NewRecordNotification).to receive(:send_sqs)
      post_valid_request
    end

    it 'triggers the AWS SQS Client to send a sqs message' do
      expect(Aws::Sqs).to receive(:send_sqs)
      post_valid_request
    end

  end

  context "tries to create invalid category" do

    context "invalid attribute" do
      let!(:invalid_category_attributes) do
        build(:category).attributes.except('name')
      end

      def post_request_with_invalid_attributes
        post "/api/v1/verticals/#{vertical.id}/categories",
             params: { category: invalid_category_attributes },
             headers: headers_with_authorisation
      end

      it 'returns a valid JSON:API standard response' do
        post_request_with_invalid_attributes
        expect(parsed_json).to be_valid_json_api_response
      end

      it 'returns status code 422' do
        post_request_with_invalid_attributes
        expect_status(:unprocessable_entity)
      end

      it 'returns json error for invalid attribute' do
        post_request_with_invalid_attributes
        expect(parsed_json).to have_json_error_on_attribute(:name)
      end

      it 'does not create a new record' do
        expect do
          post_request_with_invalid_attributes
        end.to_not change { Category.count }
      end
    end

    context "missing required param" do
      let!(:invalid_category_attributes) do
        {some_irrelevant_param: 'really_irrelevant'}
      end

      def post_request_with_missing_param
        post "/api/v1/verticals/#{vertical.id}/categories",
             params: { category: invalid_category_attributes },
             headers: headers_with_authorisation
      end

      it 'returns a valid JSON:API standard response' do
        post_request_with_missing_param
        expect(parsed_json).to be_valid_json_api_response
      end

      it 'returns status code 422' do
        post_request_with_missing_param
        expect_status(:unprocessable_entity)
      end

      it 'does not create a new record' do
        expect do
          post_request_with_missing_param
        end.to_not change { Category.count }
      end
    end

  end

end
