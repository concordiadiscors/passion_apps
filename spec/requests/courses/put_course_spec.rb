require 'rails_helper'
require_relative "../shared"

describe "post course route", :type => :request do
  include_context 'with_authenticated_user'

  let!(:course) { create(:course) }
  let!(:new_category) { create(:category, name: 'Art de Vivre') }

  let!(:course_new_attributes) do
    {
      name: 'Epicureanism 101',
      author: 'Epicure',
      category_id: new_category.id
    }
  end

  context "tries to update course with valid params" do
    def put_valid_request
      put "/api/v1/courses/#{course.id}",
          params: { course: course_new_attributes },
          headers: headers_with_authorisation
    end

    it 'returns status code 204' do
      put_valid_request
      expect_status(:no_content)
    end

    it 'does update the record' do
      expect do
        put_valid_request
      end.to change { course.reload.name }
      .and change { course.reload.category }
      .and change { course.reload.author }
    end

  end

  context "tries to update invalid course" do

    context "invalid attribute" do
      let!(:invalid_new_course_attributes) do
        course_new_attributes.merge(name: '')
      end

      def put_request_with_invalid_attributes
        put "/api/v1/courses/#{course.id}",
          params: { course: invalid_new_course_attributes },
          headers: headers_with_authorisation
      end

      it 'returns a valid JSON:API standard response' do
        put_request_with_invalid_attributes
        expect(parsed_json).to be_valid_json_api_response
      end

      it 'returns status code 422' do
        put_request_with_invalid_attributes
        expect_status(:unprocessable_entity)
      end

      it 'returns json error for invalid attribute' do
        put_request_with_invalid_attributes
        expect(parsed_json).to have_json_error_on_attribute(:name)
      end

      it 'does not update the record' do
        expect do
          put "/api/v1/courses/#{course.id}", params: { course: invalid_new_course_attributes }
        end.to not_change { course.reload.name }
        .and not_change { course.reload.category }
        .and not_change { course.reload.author }
      end
    end

    context "missing required param" do
      let!(:invalid_new_course_attributes) do
        {some_irrelevant_param: 'really_irrelevant'}
      end

      def put_request_with_invalid_attributes
        put "/api/v1/courses/#{course.id}",
            params: invalid_new_course_attributes,
            headers: headers_with_authorisation
      end

      it 'returns a valid JSON:API standard response' do
        put_request_with_invalid_attributes
        expect(parsed_json).to be_valid_json_api_response
      end

      it 'returns status code 422' do
        put_request_with_invalid_attributes
        expect_status(:unprocessable_entity)
      end

      it 'does not update the record' do
        expect do
          put "/api/v1/courses/#{course.id}", params: { course: invalid_new_course_attributes }
        end.to not_change { course.reload.name }
        .and not_change { course.reload.category }
        .and not_change { course.reload.author }
      end
    end

  end

  context "course doesn't exists" do

    let(:inexistant_id) { Course.last&.id.to_i + rand(100..1000) }

    before do
      put "/api/v1/courses/#{inexistant_id}",
          params: { course: course_new_attributes },
          headers: headers_with_authorisation
    end

    it 'returns status code 404' do
      expect_status(404)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns a 404 error' do
      expect(parsed_json).to have_json_404_error
    end

  end

end
