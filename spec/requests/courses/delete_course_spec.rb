require 'rails_helper'
require_relative "../shared"

describe "delete course route", :type => :request do
  include_context 'with_authenticated_user'

  let!(:course) { create(:course) }

  context "tries to delete existing course" do
    before { delete "/api/v1/courses/#{course.id}",
                    headers: headers_with_authorisation }

    it 'returns status code 204' do
      expect_status(:no_content)
    end

    it 'deletes the record' do
      record_match = Course.where(id: course.id).count
      expect(record_match).to eq(0)
    end

  end

  context "course doesn't exists" do

    let(:inexistant_id) { Course.last&.id.to_i + rand(100..1000) }

    before { delete "/api/v1/courses/#{inexistant_id}",
                    headers: headers_with_authorisation  }

    it 'returns status code 404' do
      expect_status(404)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns a 404 error' do
      expect(parsed_json).to have_json_404_error
    end

  end

end
