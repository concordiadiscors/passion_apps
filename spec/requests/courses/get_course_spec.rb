require 'rails_helper'
require_relative "../shared"

describe "get course route", :type => :request do
  include_context 'with_authenticated_user'

  context "course exists" do
    let!(:course) { create :course}

    before {get "/api/v1/courses/#{course.id}",
                headers: headers_with_authorisation }

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns the correct record' do
      expect(json_data).to have_id(course.id.to_s)
      expect(json_data).to have_type('courses')
    end

    it 'returns the correct attributes' do
      testable_attribute_keys = %i(name author state)
      testable_attribute_keys.each do |attr|
        expect(json_data).to have_attribute(attr).with_value(course.send(attr))
      end
    end

    it 'returns the correct relationships' do
      expect(json_data).to have_relationship(:category).with_data({
        'id' => course.category_id.to_s,
        'type' => 'categories'
      })
    end

  end

  context "course doesn't exists" do

    let(:inexistant_id) { Course.last&.id.to_i + rand(100..1000) }

    before {get "/api/v1/courses/#{inexistant_id}",
                headers: headers_with_authorisation }

    it 'returns status code 404' do
      expect_status(404)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns a json 404 error' do
      expect(parsed_json).to have_json_404_error
    end

  end
end
