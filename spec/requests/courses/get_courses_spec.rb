require 'rails_helper'
require_relative "../shared"

describe "get courses route", :type => :request do
  include_context 'with_authenticated_user'

  context "course exists" do
    let!(:courses) { create_list :course, 9}

    before {get "/api/v1/courses/",
                headers: headers_with_authorisation }

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns a valid JSON:API standard response' do
      expect(parsed_json).to be_valid_json_api_response
    end

    it 'returns the correct number of records' do
      expect(json_data.length).to be(9)
    end

    it 'returns the correct attributes' do
      testable_attribute_keys = %i(name author state)
      testable_attribute_keys.each do |attr|
        expect(json_data.first).to have_attribute(attr).with_value(courses.first.send(attr))
      end
    end

    it 'returns the correct relationships' do
      expect(json_data.first).to have_relationship(:category).with_data({
        'id' => courses.first.category_id.to_s,
        'type' => 'categories'
      })
    end

  end
end
