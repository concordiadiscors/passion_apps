require 'rspec/expectations'
require 'jsonapi/parser'

RSpec::Matchers.define :have_json_error_on_attribute do |attr|
  match do |actual|
    JSONAPI.parse_response!(actual)
    return false unless (errors = actual['errors'])
    errors.any? do |error|
      error['status'] == "422" && error['attribute'] == attr.to_s
    end
  end
end
