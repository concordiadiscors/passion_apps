require 'rspec/expectations'
require 'jsonapi/parser'

RSpec::Matchers.define :be_valid_json_api_response do
  match do |actual|
    begin
      JSONAPI.parse_response!(actual)
    rescue JSONAPI::Parser::InvalidDocument
      return false
    end
    true
  end
end
