require 'rspec/expectations'
require 'jsonapi/parser'

RSpec::Matchers.define :have_json_404_error do
  match do |actual|
    JSONAPI.parse_response!(actual)
    return false unless (errors = actual['errors'])
    errors.any? do |error|
      error['status'] == "404"
    end
  end
end
