module RequestSpecHelper
  def raw_json
    @raw_json ||= response.body
  end

  def parsed_json
    @parsed_json ||= JSON.parse(raw_json)
  end

  def json_data
    @json_data ||= parsed_json['data']
  end
end
