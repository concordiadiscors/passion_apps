require 'rails_helper'

RSpec.describe Category, type: :model do
  it "as a valid factory" do
    expect(build(:category)).to be_valid
  end

  describe "relations" do
    subject { build(:category) }
    it { is_expected.to belong_to(:vertical) }
    it { is_expected.to have_many(:courses).dependent(:destroy) }
  end

  describe "validations" do
    subject { build(:category) }
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
    it_behaves_like "with_unique_taxonomic_name"
  end



end
