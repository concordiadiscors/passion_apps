require 'rails_helper'

RSpec.describe User, type: :model do
  it "as a valid factory" do
    expect(build(:user)).to be_valid
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:email) }
  end
end
