require 'rails_helper'

RSpec.describe Vertical, type: :model do
  it "as a valid factory" do
    expect(build(:vertical)).to be_valid
  end

   describe "relations" do
    it { is_expected.to have_many(:categories).dependent(:destroy) }
  end


  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
    it_behaves_like "with_unique_taxonomic_name"
  end

end
