require 'rails_helper'

RSpec.describe Course, type: :model do
  it "as a valid factory" do
    expect(build(:course)).to be_valid
  end

  describe "validations" do
    it { is_expected.to belong_to(:category) }
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:author) }
  end

  describe "public instance methods" do
    describe "#activate" do
      context "with inactive category" do
        let!(:category) { create :category, state: :draft }
        let!(:course) { build :course, category: category, state: :draft }
        let!(:activate_event) { course.activate }
        it "return false" do
          expect(activate_event).to be false
          expect(course.errors).to_not be_empty
          expect(course.errors.messages.keys).to include(:state)
        end
      end

      context "with active category" do
        let!(:category) { create :category }
        let!(:course) { build :course, category: category, state: :draft }
        let!(:activate_event) { course.activate }
        it "return true" do
          expect(activate_event).to be true
          expect(course.errors).to be_empty
        end
      end

    end
  end
end
