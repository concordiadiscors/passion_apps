require 'rails_helper'

shared_examples_for "with_unique_taxonomic_name" do
  let(:model) { described_class }

  WithUniqueTaxonomicName.get_taxonomic_classes.each do |klass|
    it "doesn't allow for duplicate names in the #{klass.name} model" do
      other_instance = create(klass.to_s.underscore.to_sym)
      new_instance = build(model.to_s.underscore.to_sym, name: other_instance.name)
      expect(new_instance).to be_invalid
    end
  end

end
