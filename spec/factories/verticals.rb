FactoryBot.define do
  factory :vertical do
    sequence(:name) { |n| "#{Faker::IndustrySegments.industry} #{n}" }
  end
end
