FactoryBot.define do
  factory :course do
    category { create(:category) }
    name { "The Art of Eating Camembert" }
    state { :active}
    author { "Gérard Le Rustique" }
  end
end
