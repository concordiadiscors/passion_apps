FactoryBot.define do
  factory :category do
    vertical { create(:vertical) }
    sequence(:name) { |n| "#{Faker::IndustrySegments.sector} #{n}" }
    state { :active }
  end
end
