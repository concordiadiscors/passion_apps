require 'rails_helper'

RSpec.describe NewRecordNotification do

  let(:course) { create :course }
  let(:notification) { NewRecordNotification.new(course) }

  describe 'delegations' do
    %w(id name class).each do |attr|
      it "delegates #{attr} to the record object" do
        expect(course).to receive(attr)
        notification.send("record_#{attr}")
      end
    end
  end

  describe 'private formatting methods' do
    # I ususally would not really test private methods, but because of the current lack of an
    # actual functional public one for this class, I thought I would add a few specs here....

    describe '#message_body' do
      it 'creates a message body' do
        expected_message = "A new Course has been created: #{course.name}"
        expect(notification.send(:message_body)).to eq(expected_message)
      end
    end

    describe '#readable_record_type' do
      it 'formats the record class in a human readable way' do
        expect(notification.send(:readable_record_type)).to eq('Course')
      end
    end

    describe '#message_attributes' do
      it 'creates a hash of message attributes' do
        expected_hash = {
            'Type' => {
              string_value: 'Course',
              data_type: 'String'
            },
            'ID' => {
              string_value: course.id.to_s,
              data_type: 'Number'
            },
            'Name' => {
              string_value: course.name,
              data_type: 'String'
            }
          }

          expect(notification.send(:message_attributes)).to eq(expected_hash)
      end
    end
  end


end
