# README

## Setup

### Github Oauth credentials
The Oauth client for Github will require environment variables to be set.
The gem [figaro](https://github.com/laserlemon/figaro) is set, and adding variables in a `config/application.yml` file would do the trick, like:

```yaml
# Add configuration values here, as shown below.

test:
  GITHUB_OAUTH_CLIENT_ID: 'fake_client_id'
  GITHUB_OAUTH_CLIENT_SECRET: 'fake_client_secret'

```

### Database
The `database.yml` file is git-ignored, so a new one should be created with correct parameters by duplicating the `config/database\_template.yml`

## Models
### Name Constraint on Vertical and Category
I saw 3 different approaches to apply that constraint:

#### Option 1: Using a Single Table Inheritance approach
This is what would have been my favoured option, because:
- it seems to make sense from a business approach, the `Vertical` and `Category` model are both taxonomies, really similar in nature, etc...
- it is easy and natural to implement from a DB/Active Record perspective

A `Taxonomy` model could be implemented, with a unique contraint on the name column of the table, e.g.

```ruby
class Taxonomy < ApplicationRecord
  validates :name, presence: true, uniqueness: true
end

class Vertical < Taxonomy
  # stuff specific to Vertical
end

class Category < Taxonomy
  # stuff specific to Category
end
```

with a migration which might look like that, with a unique constraint on the `name` column:

```ruby
  create_table "taxonomies", force: :cascade do |t|
    t.string "name"
    # and all the other stuff we might need e.g parent keys, etc...
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_taxonomies_on_name", unique: true
  end
```

However, the fact there is already a `state` in the `Category` that is missing from the `Vertical` model, and that I don't know how far both models might actually diverge in nature in the future (it is quite conceivable that we might get many more columns specific to either the `Category` or the `Vertical` models), prompted me to ignore that approach.

#### Option 2: Extracting the column and constraint to a different model/table
Another possibility would have been to extract the `name` attribute to polymorphic model, e.g. `TaxonomyDetail`, which would belong to either the `Category` or the `Vertical` model, and add the unique constraint to the `name` column of that newly created table.

Should the need for more common attributes arise, this model could be a good place to add them.

I somehow felt though that the name might be seen as an inherent attribute of the `Category` and `Vertical` models, and creating a model/table for the sole purpose of guaranteeing the uniqueness of the name might be not be fully justified from a domain perspective.

#### Option 3:  Custom validation
The solution that I adopted ended up adding a custom validation through the `WithUniqueTaxonomicName` concern.
Similar to what the ActiveRecord's uniqueness validation would do, a single custom query is compiled and checks if there are any record across all the tables of the models with the same name as the one being validated.

A problem with that could be a race condition:
2 different processes checking within the same small time window that said the uniqueness is valid, and inserting 2 records with the same name

Since we can't have a unique constraint on multiple tables (which would help with this issue), I created validation function and triggers which would prevent duplicate names to be inserted across these tables in the `AddNameValidationTriggersOnDb` migration

```sql
 CREATE OR REPLACE FUNCTION check_taxonomic_name_uniqueness_func()
      RETURNS TRIGGER
      AS $$
      BEGIN
      IF exists(
              select 1 from (
               select id, name, 'categories' as tbl from categories
               union
               select id, name, 'verticals' as tbl from verticals
              ) all_taxonomies where all_taxonomies.name = NEW.name and (
                      all_taxonomies.id != NEW.id OR
                      all_taxonomies.tbl != TG_TABLE_NAME
              )

      ) THEN
         RAISE EXCEPTION 'taxonomy name must be unique';
      END IF;
      RETURN NEW;
      END;
      $$
      LANGUAGE plpgsql;

      CREATE TRIGGER validates_category_name_uniqueness_trigger
      BEFORE INSERT OR UPDATE ON categories
      FOR EACH ROW EXECUTE PROCEDURE check_taxonomic_name_uniqueness_func();

      CREATE TRIGGER validates_vertical_name_uniqueness_trigger
      BEFORE INSERT OR UPDATE ON verticals
      FOR EACH ROW EXECUTE PROCEDURE check_taxonomic_name_uniqueness_func();
```



It is a bit clunky in the sense that triggers need to be managed through migrations everytime the concern is added/removed.

To ease that process, something like the gem [hair-trigger](https://github.com/jenseng/hair_trigger) could be used (I didn’t use that gem yet as just found out about it , but will definitely play with it very soon)

### State Machine
The `state` attributes in the JSONs seemed to indicate that a state machine was required in the `Category` and `Course` models.
I implemented these quickly with the `state_machines-activerecord`, and also took the liberty to add the `draft` and `archived` states.

## Controllers, API, and Authentication
### Routes
Nothing spectular here I guess, I had the `categories` resources nested in the `verticals`, as it seemed a logical structure.

Re: courses, I decided not to nest them (3 nested levels start to be lot), and I could also see a future possibility for courses to have many categories.

### Controllers & Serializers
I guess the implementation of the controllers is pretty typical here.

Re: serializers, I opted to use the JSON:API standard as it is a well-defined, widely adopted standard, covering all an API needs.
It is implemented here through ActiveModel::Serializer.

Should the need for the JSON schema to actually conform to the JSONs files provided in the repository, it would just be a matter of modifying quickly the serializers and the `config/initializers/active_model_serializer.rb` initializer

### Authentication
Re: the Bonus clause with Oauth, I wasn't 100% sure whether the request was to:
- set an existing Oauth provider (e.g. Twitter, Github, etc..) to secure this app's API,
- or to actually create a Oauth provider

I thought it was more likely to be the former, so I elected to use Github as a Oauth provider.
If I had to actually create a Oauth provider for that app, I would have most likely use the `doorkeeper` gem.

I implemented here the server-side functionalities for a _Authorization Code Grant_ modality where:
1. The front-end sends user to the Oauth login page of Github, using the the app's client_id
2. After all credentials are verified, Github redirects the user to front-end app with a authorization code
3. Front-End sends said authorization code to back-end
4. Back-end, using aforementioned authorization code, in conjunction with the app's Oauth secret, is going to request a token
5. token can then be used for upcoming requests to Github's resource server

In this implementation, the Github API is accessed during the authentication process (`app/controllers/api/v1/authentications_controller.rb`) to match the logged Github user's email to an existing user email in the app.

Upon success, a JWT token is then generated and returned as a response.
The JWT token will then be used to authenticate the user on subsequent requests.

Please note that the user model here is *really* barebone, and solely used to implement this authentication process.

## Questions
### What would you improve next?
My very first step would be to enquire more about the business models with the peers/product owners/shareholders, etc...

For instance, I'm not quite sure what the authenticated user's role is, and whether he is actually entitiled to create, edit, delete verticals/categories/courses and if he is, should then these be scoped to the currently logged user (with the addition of new foreign keys, etc...)?

In the same spirit, I would examine the front-end application use, to determine what the most common uses are.

Re: user and authentication, given that it aligns with the business requirements, I would probably also implement a more comprehensive solution, adding a login with password, a signup page, and more fleshed-out user model.

I would also add more features in terms of API, for instance add facilities to cater for [pagination](https://jsonapi.org/format/#fetching-pagination), [filtering](https://jsonapi.org/format/#fetching-filtering), [included resources](https://jsonapi.org/format/#fetching-includes), [fields](https://jsonapi.org/format/#fetching-sparse-fieldsets) etc... as to make the overall API more performant, and friendlier to use for the clients.

Speaking of performance, I would also build a caching mechanism for the JSON API responses (probably using Redis), and ensure that queries are not unduly run when the cache is hit.

### How does your solution perform?
I believe given the requirements, the solution performs adequately (although I did not really run performances tests as such).

Again, running it against an actual client would greatly help to figure out what bottleneck might arise:
for instance, should a a `GET course` response include the JSON objets of the related category and vertical (thus avoiding extra requests) – this could be done using the [includes](https://jsonapi.org/format/#fetching-includes) param of the JSON:API.

### How does your solution scale?
In its current state, separation of concerns is somehow ensured, and the amount of code present is fairly small, with a comprehensive test suite, so I would say that the codebase is maintainable (therefore scalable).

As mentioned above, adding caching would greatly enhance performance, as well as adding features like pagination, filtering or sparse fieldsets to reduce the load on the server and the size of the JSON responses.











