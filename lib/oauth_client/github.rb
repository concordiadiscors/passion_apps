module OauthClient::Github

  extend self

  def client
    # client settings would probably be defined elsewhere, e.g initializer
    @client ||= OAuth2::Client.new(
      ENV['GITHUB_OAUTH_CLIENT_ID'],
      ENV['GITHUB_OAUTH_CLIENT_SECRET'],
      site: 'https://github.com',
      authorize_url: 'https://github.com/login/oauth/authorize',
      token_url: 'https://github.com/login/oauth/access_token'
    )
  end

  def user_from_auth_code(code)
    begin
      token = client.get_token(code: code)
    rescue OAuth2::Error
      # rescue could probably be enhanced with some logging, etc...
      # but that will do for tnow
      return false
    end
    json = token.get('https://api.github.com/user').body
    OauthClient::Github::User.new(json)
  end

  class User
    def initialize(json)
      @json = json
    end

    def attributes
      JSON.parse(@json).deep_symbolize_keys
    end

    def email
      attributes[:email]
    end

  end



end
