module Aws::Sqs

  # obvioulsy loads is missing here, just leaving these guys as mock and possible
  # skeleton for actual implementation

  extend self

  def send_sqs(queue:, message_body: , message_attributes: {})
    # would  need to log/report exceptions here

    # begin
    #   client.send_message({
    #      queue_url: queue_url,
    #      message_body: message_body,
    #      message_attributes: message_attributes
    #   })
    # rescue Aws::SQS::Errors::NonExistentQueue
    #   puts "A queue named '#{queue_name}' does not exist."
    #   exit(false)
    # end
  end

  def client
    # this could probably be memoized
    # also, client settings should probably be defined in initializer
    # @client ||=  Aws::SQS::Client.new(region: 'eu-west-1')
  end

end
