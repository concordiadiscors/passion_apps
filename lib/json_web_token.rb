require "jwt"

module JsonWebToken
  extend self

  def encode(payload, expiration: 12.hours.from_now)
    payload[:expiration] = expiration.to_i
    JWT.encode(payload, Rails.application.secrets.secret_key_base)
  end

  def decode(token)
    JWT.decode(token, Rails.application.secrets.secret_key_base).first.deep_symbolize_keys
  end
end
